/*
 ============================================================================
 Name        : huffman.c
 Author      : Micah Halter
 Version     : 1.0
 ============================================================================
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "huffman.h"

/*
 * printUsage() - prints how our program is to
 * be used from the command line.
 */
void printUsage() {
    //puts the usages to the screen in an organized fashion
    puts("Usage: huffman inputFile      [outputFile.huff]");
    puts("       huffman inputFile.huff [outputFile]");
}

/*
 * hasHus(char* string) - checks if the string ends in
 * ".huff", therefore indicating a compressed file
 * return - a 1 if the string ends in ".huff" and 0 otherwise
 */
int hasHuff(char* string) {
    //check if the substring after the last '.' in the past string is ".huff"
    return strrchr(string, '.') && !strcmp(strrchr(string, '.'), ".huff");
}

/*
 * newNode(int* ints, int arrsize, int count, string Node* l, struct Node* r) -
 * creates a new node struct based on the provided information in the
 * constructor
 * return - a pointer to the newly created node struct
 */
struct Node* newNode(int* ints, int arrsize, int count, struct Node* l, struct Node* r) {
    //malloc the new node to be returned
    struct Node* out = malloc(sizeof(struct Node));
    //malloc for the int array contents of the node
    int* inputInts = malloc(sizeof(int) * arrsize);
    //copy the ints into the malloc
    for(int i = 0; i < arrsize; i++)
        inputInts[i] = ints[i];
    //set the node variables
    out->contents = inputInts;
    out->size = arrsize;
    out->freq = count;
    out->left = l;
    out->right = r;
    //return the node
    return out;
}

/*
 * newBaseNode(int* ints, int count) - creates a new node struct
 * with the given string and count, and sets the left and right nodes
 * to NULL indicating placement on the bottom of the tree
 * return - a pointer to the newly created node struct
 */
struct Node* newBaseNode(int* ints, int count) {
    //return a new Node with the left and right nodes as NULL and a size of 1
    return newNode(ints, 1, count, NULL, NULL);
}

/*
 * getTree(struct NodeArray* freq) - constructs a tree from the passed
 * in NodeArray struct by organizing them by frequency counts
 * return - a node struct of the top most node in the tree
 */
struct Node* getTree(struct NodeArray* list) {
    //if the size of the array is greater than 1, then the tree can still grow
    if (list->size > 1) {
        //gets the 2 smallest nodes in the list
        int* smallest = getSmallest(list);
        //create a new int array that is the two contents of the smallest nodes together
        int newInts[list->nodes[smallest[1]]->size + list->nodes[smallest[0]]->size];
        //add the two int arrays together
        int place = 0;
        for(int i = 0; i < list->nodes[smallest[1]]->size; i++)
            newInts[place++] = list->nodes[smallest[1]]->contents[i];
        for(int i = 0; i < list->nodes[smallest[0]]->size; i++)
            newInts[place++] = list->nodes[smallest[0]]->contents[i];
        //set the index of the first smallest node to a new node connecting the two smallest nodes together
        list->nodes[smallest[1]] = newNode(newInts, list->nodes[smallest[1]]->size + list->nodes[smallest[0]]->size, list->nodes[smallest[1]]->freq + list->nodes[smallest[0]]->freq, list->nodes[smallest[1]], list->nodes[smallest[0]]);
        //move the memory of the list, removing the extra node
        memmove(list->nodes + smallest[0], list->nodes + smallest[0] + 1, (list->size - smallest[0] - 1) * sizeof(struct Node*));
        //decrease the size of the list
        list->size--;
        //recursively call getTree with the new, shortened list until the list is down to one element
        return getTree(list);
    }
    //if the size of the array is 1, return the element in the array
    //this represents the top of the tree
    return list->nodes[0];
}

/*
 * getBitArray(struct Node* tree) - constructs a malloced array of every bit
 * pattern stored in the tree passed in
 * return - the malloced array that was created
 */
char** getBitArray(struct Node* tree) {
    //creates a malloc of the array
    char** array = malloc(sizeof(char*) * 257);
    //loops through every ascii value and adds
    //it to the array
    for(int i = 0; i < 257; i++)
        array[i] = getCode(tree, i, NULL);
    //returns the array
    return array;
}

/*
 * getCode(struct Node* next, int find, char* bits) - returns a malloced
 * string of the bits needed to code the character find. Takes in the node
 * struct of the tree, the int of the code you want to find, and NULL
 * as the char* bits to help with the recursive function
 * return - the malloced char* of the bit pattern needed to code the char find
 */
char* getCode(struct Node* next, int find, char* bits) {
    //check if the left and right nodes are initialized to check
    //if the next node is a base node
    if (next->left && next->right) {
        //if bits is null, malloc an empty string to be built
        if (!bits) {
            bits = malloc(1);
            strcpy(bits, "");
        }
        //check if the left node contains the looking variable and add a 0 or 1
        //based on it being in the left or right node
        int found = 0;
        for(int i = 0; i < next->left->size; i++)
            if(next->left->contents[i] == find) found = 1;
        char* newChar = found ? "0" : "1";
        //set next to the next node in the tree
        next = strcmp(newChar, "0") == 0 ? next->left : next->right;
        //resize the bit string and add the new bit to the end of the string
        bits = realloc(bits, strlen(bits) + 2);
        strcat(bits, newChar);
        //recursively call the function to build each new bit until it reaches
        //the bottom of the tree
        return getCode(next, find, bits);
    }
    //if it is a base node, return the bit string that was created
    return bits;
}

/*
 * getSmallest(struct NodeArray* frequencies) - returns a int array of size 2
 * containing the indexes of the 2 smallest nodes in the node array with a
 * frequency greater than 0
 * return - int* of the indexes of the 2 smallest non-zero frequency nodes
 */
int* getSmallest(struct NodeArray* freq) {
    //create a static int array of 2 to add to
    static int out[2];
    //add the starting indexes to the output array
    out[0] = 0;
    out[1] = 0;
    //loop through the node array
    for (int i = 0; i < freq->size; i++)
        //if the frequency of the current node is less than the frequency of the node
        //at the saved first index, then set the second index to the old first index
        //and set the current index to the first index
        if (freq->nodes[i]->freq <= freq->nodes[out[0]]->freq) {
            out[1] = out[0];
            out[0] = i;
        }
    //if the second and first index are the same, then reloop through, skipping the first index
    //and find the second smallest frequency
    if (out[1] == out[0]) {
        out[1] = out[0] == 0 ? 1 : 0;
        for(int i = 0; i < freq->size; i++)
            if (i != out[0] && freq->nodes[i]->freq < freq->nodes[out[1]]->freq)
                out[1] = i;
    }
    //return the array of smallest indexes
    return out;
}

/*
 * getFrequency(char* filePath) - creates a node array of base nodes and their
 * frequencies from the compressed file at 'filePath'
 * return - a NodeArray struct of the base nodes and their frequencies
 */
struct NodeArray* getFrequency(char* filePath) {
    //opens the input file as readable
    FILE* fp = fopen(filePath, "r");
    //creates a list to store the ascii frequencies the size of
    //the ascii table initialized at 0s
    int* list = calloc(257, sizeof(int));
    //set holder variables to mark place throughout the file
    int place = 0;
    //set holder variable to count size of non-zero ascii frequencies
    int size = 0;
    //create new empty string as a character buffer to build integers
    char* temp = malloc(1);
    strcpy(temp, "");
    //keep track or first and second previous characters to know if you are at a
    //character or part of one of the numbers
    int prevPrevChar = -1;
    int prevChar = -1;
    //keep track of the new character that we are saving the frequency read as
    int newChar = -1;
    //loop through ever character in the file
    for(int ch = fgetc(fp); ch != EOF; ch = fgetc(fp)) {
        //if the place is 0 and the character is ' ' then we are at the end of the size
        //integer
        if(place == 0 && ch == ' ') {
            //set the size of the current integer string saved in the temp variable
            size = atoi(temp);
            //free temp and reinitialize as an empty string
            free(temp);
            temp = malloc(1);
            strcpy(temp, "");
            //increase the place by 1
            place++;
        }
        //if the place is greater than the size, then all the known ascii frequencies
        //have been read and the loop breaks
        else if(place > size) break;
        //if the character is ' ' and the previous character isn't a ' ', then we are at the end
        //of a number, and we need to save it as a frequency value
        else if (ch == ' ' && prevChar != ' ') {
            //store the grabbed integer string in the space of the newCharacter place in the list
            list[newChar] = atoi(temp);
            //increment the place variable
            place++;
            //free the temp string and reinitialize as an empty string
            free(temp);
            temp = malloc(1);
            strcpy(temp, "");
        //if the previous character is ' ' and the second previous character isn't a ' ', then we
        //are at the next character whose frequency we will find
        } else if(prevChar == ' ' && prevPrevChar != ' ') {
            //save the current character as the newChar
            newChar = ch;
        //everything else is a number that we need to add to the integer temp buffer
        } else {
            //resize the temp string
            temp = realloc(temp, strlen(temp) + 2);
            //add the character to the end of the temp string
            strcat(temp, (char*) &ch);
        }
        //move the old characters down a space to keep track of the most updated previous and previous previous character
        prevPrevChar = prevChar;
        prevChar = ch;
    }

    //close the read file
    fclose(fp);
    free(temp);

    //add our sudo EOF character to the list
    list[256] = 1;
    //increment the size based on this change
    size++;

    //convert the list of frequencies to an array of nodes of ascii values of more than a 0 frequency
    struct NodeArray* freq = arrayFromList(list, size);

    //free the temp list and temp string
    free(list);

    //return the node array built from the list
    return freq;
}

/*
 * getFrequencyFile(char* filePath, char* outputPath) - calculates the character
 * frequencies of the given input file 'filePath' and either stores them into the
 * file 'outputPath', or does not write it to a file if the outputPath is NULL
 * return - a NodeArray struct of the base nodes and their frequencies
 */
struct NodeArray* getFrequencyFile(char* filePath, char* outputPath) {
    //open the inputted file for reading
    FILE* fp = fopen(filePath, "r");
    //create a list to add the frequencies to the size of the ascii table
    //so the index corresponds to the character the integer frequency goes to
    int* list = calloc(257, sizeof(int));
    //initialize the number of non-zero frequencies to 0
    int size = 0;
    //loop through each character in the file
    for(int ch = fgetc(fp); ch != EOF; ch = fgetc(fp)) {
        //if the current frequency of the character in the list is 0
        //increase the size because it is no longer going to be 0
        if (list[ch] == 0) size++;
        //increment the frequency of the current character
        list[ch]++;
    }
    //clase the input file since we are finished with it
    fclose(fp);
    //add our sudo EOF character to the list
    list[256] = 1;
    //increase the size because of the added frequency
    size++;
    //convert the list of frequencies to an array of base nodes
    struct NodeArray* freq = arrayFromList(list, size);

    //check if an output file was passed
    if(outputPath) {
        //if so, open it for writing
        FILE* op = fopen(outputPath, "w");
        //write the number of non-zero frequencies to the top of the file
        fprintf(op, "%d ", size - 1);
        //loop throught the list of frequencies
        for(int i = 0; i < 256; i++)
            //if the frequency isn't 0, store the character and its frequency in the file
            if(list[i] != 0) fprintf(op, "%c%d ", i, list[i]);
        //close the file after being used
        fclose(op);
    }

    //free the list from being used
    free(list);

    //return the build array of base nodes
    return freq;
}

/*
 * arrayFromList(int* list, int size) - takes the given int array and converts
 * the values to an array of base nodes where there are 'size' characters with
 * non-zero values
 * return - a NodeArray struct of the base nodes and their frequencies
 */
struct NodeArray* arrayFromList(int* list, int size) {
    //malloc a new node array struct
    struct NodeArray* freq = malloc(sizeof(struct NodeArray));
    //create a malloc for the array of nodes the size of the passed in size
    freq->nodes = malloc(sizeof(struct Node) * size);
    //set a temp variable to hold place in the node array
    int place = 0;
    //loop through the list passed in
    for(int i = 0; i < 257; i++)
        //if the frequency isn't 0 convert it to a new base node and add it
        //it to the node array
        if(list[i] != 0)
            freq->nodes[place++] = newBaseNode((int*) &i, list[i]);
    //set the node array size to the passed in size
    freq->size = size;

    //return the node array
    return freq;
}

/*
 * encode(char* inputPath, char* outputPath, char** array) - takes the
 * file at 'inputPath' and converts each character to their corresponding
 * bit string and writes those bits to the file at 'outputPath' unless the
 * file is NULL, then the bits will be printed to the screen
 */
void encode(char* inputPath, char* outputPath, char** array) {
    //open the input file for reading
    FILE* rf = fopen(inputPath, "r");
    //open the output file as an append so that we don't overwrite the character frequencies
    //if a file isn't passed in, then we just set the file to NULL
    FILE* wf = outputPath ? fopen(outputPath, "a") : NULL;
    //initialize an empty string to act as the bit buffer
    char* currentBits = malloc(1);
    strcpy(currentBits, "");
    //loop through every character in the file
    for (int ch = fgetc(rf); ch != EOF; ch = fgetc(rf)) {
        //increase the size of the bit buffer to add the new bits
        currentBits = realloc(currentBits, strlen(currentBits) + strlen(array[ch]) + 1);
        //add the new bits to the bit buffer
        strcat(currentBits, array[ch]);
        //if and while the strlen of the bit buffer is greater than 8, we have full characters
        //we can write to the file, and we want to keep the bit buffer as small as we can
        while(strlen(currentBits) >= 8) {
            //we create a temp bit string to hold the first 8 bits in the bit buffer
            char bitString[9];
            //copy the first 8 bits of the bit buffer to the temp bit string
            strncpy(bitString, currentBits, 8);
            //set the terminating character of the bit string
            //remove the first 8 bits from the bit buffer
            memmove(currentBits, currentBits + 8, strlen(currentBits) - 7);
            //if the output file isn't NULL conver the bit string to a base 10 character and
            //write that character to the file
            if (wf) fputc(strtol(bitString, NULL, 2), wf);
            //if the output file is NULL, then we just print the bits to the screen
            else printf("%s", bitString);
        }
    }
    //clase the file since we are done reading it
    fclose(rf);
    //increase the size of our bit buffer to hold the new bits
    currentBits = realloc(currentBits, strlen(currentBits) + strlen(array[256]) + 1);
    //add the new bits to the bit buffer
    strcat(currentBits, array[256]);
    //while the length of the bit buffer isn't divisible by 8
    while(strlen(currentBits) % 8 != 0) {
        //add a 0 to the end of the bit buffer to pad the end with zeros so that it is writable
        //as characters
        currentBits = realloc(currentBits, strlen(currentBits) + 2);
        strcat(currentBits, "0");
    }
    //while the strlen of the bit buffer is 8 or greater write the bits so that we write every bit needed
    while(strlen(currentBits) >= 8) {
        //we create a temp bit string to hold the first 8 bits in the bit buffer
        char bitString[9];
        //copy the first 8 bits of the bit buffer to the temp bit string
        strncpy(bitString, currentBits, 8);
        //remove the first 8 bits from the bit buffer
        memmove(currentBits, currentBits + 8, strlen(currentBits) - 7);
        //if the output file isn't NULL conver the bit string to a base 10 character and
        //write that character to the file
        if (wf) fputc(strtol(bitString, NULL, 2), wf);
        //if the output file is NULL, then we just print the bits to the screen
        else printf("%s", bitString);
    }
    //if the bits are being printed to the screen, add a new line for better viewing
    if(!wf) puts("");
    //free the bit buffer
    free(currentBits);
    //if we are writing to a file, close the file we used for writing
    if (wf) fclose(wf);
}

/*
 * decode(char* inputPath, char* outputPath, struct Node* tree, int size) -
 * takes the file at 'inputPath' and converts the bits back to the corresponding
 * characters from the tree passed in, also takes in the size of the tree to know
 * how many places to skip for the letter frequencies stored in the file
 */
void decode(char* inputPath, char* outputPath, struct Node* tree, int size) {
    //open the input file for reading
    FILE* rf = fopen(inputPath, "r");
    //open the output file as an append so that we don't overwrite the character frequencies
    //if a file isn't passed in, then we just set the file to NULL
    FILE* wf = outputPath ? fopen(outputPath, "w") : NULL;
    //set a current node at the top of the tree to help follow the tree down to a character from bits
    struct Node* currentNode = tree;
    //add a counter to keep track of first few lines to skip the character frequencies stored in the
    //compressed file
    int counter = 0;
    //initialize the previous character to -1
    int prevChar = -1;
    //loop through each character in the input file
    for(int ch = fgetc(rf); ch != EOF; ch = fgetc(rf)) {
        //if the character is ' ', the previous character isn't ' ', and the counter is less than the size + 1,
        //then increase the counter because we are still in the frequency part of the file
        if(ch == ' ' && prevChar != ' ' && counter < size) counter++;
        //once the counter is greater than the size + 1, we are outside of the frequencies portion of the file
        else if(counter >= size)
            //loop through each bit in the current character
            for(int j = 0; j < (8*sizeof(int) + 1); j++) {
                //if the tree is at a base node, then we need to get the current character
                if (!currentNode->left && !currentNode->right) {
                    //if the decompressed character is our pseudo EOF character
                    if(currentNode->contents[0] == 256) {
                        //close reading file
                        fclose(rf);
                        //if we have a writing file, we close that as well
                        if(wf) fclose(wf);
                        //return out of the function
                        return;
                    }
                    //if the character isn't our pseudo EOF character we continue
                    //if we have a output file
                    if(wf)
                        //add the new character to the end of the file
                        fprintf(wf, "%c", currentNode->contents[0]);
                    //if we don't have an output file
                    else
                        //print the character to the screen
                        printf("%c", currentNode->contents[0]);
                    //set the current node to the top of the tree
                    currentNode = tree;
                }
                //loop through the last 8 bits of the integer so that we only get the character bits
                if(j > 23 && j < 32)
                    //set the current node to the left or right depending on if the bit is a 1 or 0 respectively
                    currentNode = (ch << j) & (1 << (8*sizeof(int)-1)) ? currentNode->right : currentNode->left;
            }
        //set the previous character to the what is now the current character
        prevChar = ch;
    }
}

/*
 * freeTree(struct Node* tree) - loops through a node tree and frees every
 * node within its structure and all of the contents of the node
 */
void freeTree(struct Node* tree) {
    //if there is a left and right node
    if (tree->left && tree->right) {
        //free the left node recursively
        freeTree(tree->left);
        //free the right node recursively
        freeTree(tree->right);
        //free the contents of the current node
        free(tree->contents);
        //free the node itself
        free(tree);
    //if it is a base node
    } else {
        //free its contents
        free(tree->contents);
        //free itself all together
        free(tree);
    }
}

/*
 * freeNodeArray(struct Node* tree) - frees all of the properties of the
 * NodeArray struct tree, and all of the nodes inside of it
 */
void freeNodeArray(struct NodeArray* arr) {
    //loop through the array of nodes and free each node tree inside of it
    for (int i = 0; i < arr->size; i++)
        freeTree(arr->nodes[i]);
    //free the array of nodes
    free(arr->nodes);
    //free the node array struct itself
    free(arr);
}

/*
 * freeBitArray(char** arr) - loops through a bit array and frees every
 * string within it and the array itself
 */
void freeBitArray(char** arr) {
    //loop through the array of string and free each string inside of it
    for(int i = 0; i < 257; i++)
        free(arr[i]);
    //free the array itself
    free(arr);
}
