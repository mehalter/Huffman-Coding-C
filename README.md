-----
README
-----

== Huffman-Coding-C ==

Creator : Micah Halter

Version 1.00 22 May 2015

Requires: C-Compiler (Make file built for clang-3.5 or gcc)

======================================================================
I. Description
--------------

Compiling:

1. **~$ make**

Running:

1. Compress:   **~$ ./huffman inputFile outputFile.huff**
2. Decompress: **~$ ./huffman inputFile.huff outputFile**



This is a C implementation of the Huffman coding compression algorithm for my Data Structures I class.

II. Included
------------

- README.md
- LICENSE
- Makefile
- huffman.h
- huffman.c
- main.c


======================================================================
Contact
-------

Question, Comments, and Bugs at:

- micah@mehalter.com
