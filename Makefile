CC=gcc
CFLAGS=-std=c99 -o
#CC=clang-3.5
#CFLAGS=-o

EXECUTE=huffman

all: $(EXECUTE)

$(EXECUTE): main.c $(EXECUTE).c
	$(CC) $(CFLAGS) $(EXECUTE) main.c $(EXECUTE).c

clean:
	rm -f $(EXECUTE)
