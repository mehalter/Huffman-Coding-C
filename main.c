/*
 * main.c
 *
 *  Created on: May 30, 2015
 *      Author: micah
 */

#include<stdio.h>
#include<unistd.h>
#include "huffman.h"

int main(int argc, char** argv) {
    //check if there is the correct number of arguments
    if(argc < 2 || argc > 3) {
        //if not, print the usage and return an exit code of 1
        printUsage();
        return 1;
    //check if one of the files is a compressed '.huff' file
    } else if(argc == 3 && !(hasHuff(argv[1]) || hasHuff(argv[2]))) {
        //if not, print the usage and return an exit code of 1
        printUsage();
        return 1;
    //make sure that both files aren't compressed '.huff' files
    } else if(argc == 3 && hasHuff(argv[1]) && hasHuff(argv[2])) {
        //if not, print the usage and return an exit code of 1
        printUsage();
        return 1;
    //check if the input file exists to avoid a seg fault
    } else if(access(argv[1], F_OK ) == -1) {
        //if not, print the file doesn't exist and return an exit code of 1
        printf("%s does not exist\n", argv[1]);
        return 1;
    }
    //create unitialized variables to hold our node array and tree node
    struct NodeArray* x;
    struct Node* top;
    //if the first file passed is a compressed file we know we must decompress
    if(hasHuff(argv[1])) {
        //get the frequency node array from the compressed file and store it as x
        x = getFrequency(argv[1]);
        //save the size of the node array
        int size = x->size;
        //get the tree from the node array and store it as top
        top = getTree(x);
        //decode the input file to the passed in file
        //if an output file wasn't passed, we just print to the screen
        decode(argv[1], argv[2], top, size);
    //if the first file isn't a compressed file, we need to compress the given file
    } else {
        //get the frequency from the node array and if an output file is passed,
        //write the frequencies to that file
        x = getFrequencyFile(argv[1], argv[2]);
        //get the tree from the node array and store it as top
        top = getTree(x);
        //build the array of bit strings
        char** array = getBitArray(top);
        //compress the input file, and save it to the output file is one is passed
        //it one isn't, then we just print the bits to the screen
        encode(argv[1], argv[2], array);
        //free the bit array that was constructed
        freeBitArray(array);
    }
    //free the node array x which in turn frees the tree stored in top
    freeNodeArray(x);
    //return an exit code of 0 to indicate a successful run
    return 0;
}
